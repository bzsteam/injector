/**
 * Created by aditya on 19/04/17.
 */

let express = require('express');
let cors = require('cors');

let app = express();
let port = 8000;

let corsOptions = {
    origin: true,
    credentials: true
};
app.use(cors(corsOptions));

app.use('/api/v1',express.static(__dirname + '/dist'));
app.listen(port, function () {
    console.log(`Static Server listening on port ${port}!`)
});